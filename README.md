# Directories
This script simulates being a file manager. To start using it you should have installed node 16.9.11, if you are using `nvm` just run `nvm use`.

## Requirements
Before you can run this project you need to install some dependencies:
- Install node 16.9.11, if you are using `nvm` run `nvm use` inside this directory.
- Install dependencies with `npm i`.

## How to test it?
You can run unit test for class `Tree.js` by running `npm test`.

For running the project try to run either `npm start` or `node index.js`, it will display the following output:

```
CREATE fruits
CREATE vegetables
CREATE grains
CREATE fruits/apples
CREATE fruits/apples/fuji
LIST
fruits
  apples
    fuji
grains
vegetables

CREATE grains/squash
MOVE grains/squash vegetables
CREATE foods
MOVE grains foods
MOVE fruits foods
MOVE vegetables foods
LIST
foods
  fruits
    apples
      fuji
  grains
  vegetables
    squash

DELETE fruits/apples
Cannot delete fruits/apples - fruits does not exist
DELETE foods/fruits/apples
LIST
foods
  fruits
  grains
  vegetables
    squash
```
