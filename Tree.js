class Tree {
  constructor () {
    this.path = {}
  }

  _findPath (object, paths) {
    const p = paths.shift()
    const obj = object[p]
    if (!obj) return new Error(p)
    if (paths.length > 0) {
      return this._findPath(obj, paths)
    }
    return obj
  }

  listPaths (tab = '', object) {
    object = object || this.path
    return Object.keys(object).sort().reduce((acc, item) => {
      acc += `${tab}${item}\r\n`
      if (Object.keys(object[item]).length > 0) {
        acc += this.listPaths(`${tab}  `, object[item])
      }
      return acc
    }, '')
  }

  addPath (path, object) {
    const paths = path.split('/')
    const p = paths.shift()
    object = object || this.path
    object[p] = object[p] || {}
    if (paths.length > 0) {
      this.addPath(paths.join('/'), object[p])
    }
  }

  movePath (origin, destination) {
    const origins = origin.split('/')
    const pathName = origins[origins.length - 1]

    const p1 = this._findPath(this.path, origins)
    const p2 = this._findPath(this.path, destination.split('/'))

    if (p1 instanceof Error) {
      console.error(`Cannot move ${origin} - ${p1.message} does not exist`)
      return
    }
    if (p2 instanceof Error) {
      console.error(`Cannot move ${origin} - ${p2.message} does not exist`)
      return
    }

    p2[pathName] = JSON.parse(JSON.stringify(p1))
    const tmpPath = origin.split('/')
    tmpPath.pop()
    const parentObject = tmpPath.length > 0 ? this._findPath(this.path, tmpPath) : this.path
    Reflect.deleteProperty(parentObject, pathName)
  }

  removePath (path) {
    const paths = path.split('/')
    const pathName = paths[paths.length - 1]
    const p = this._findPath(this.path, paths)

    if (p instanceof Error) return console.error(`Cannot delete ${path} - ${p.message} does not exist`)

    const tmpPath = path.split('/')
    tmpPath.pop()
    const parentObject = tmpPath.length > 0 ? this._findPath(this.path, tmpPath) : this.path
    Reflect.deleteProperty(parentObject, pathName)
  }
}

module.exports = Tree
