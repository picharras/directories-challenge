const Tree = require('../Tree')

describe('Tree', () => {
  const consoleSpy = jest.spyOn(console, 'error')
  test('should create an instance of Tree', async () => {
    const obj = new Tree()

    expect(obj.constructor.name).toBe(Tree.name)
    expect(obj).toHaveProperty('path', {})
  })

  test('should add a nested paths', () => {
    const obj = new Tree()
    obj.addPath('food/mexican/tacos')

    expect(obj.path).toEqual({ food: { mexican: { tacos: {} } } })
  })

  test('should move a path to other one', () => {
    const obj = new Tree()
    obj.addPath('food/mexican/tacos')

    obj.addPath('fruits')

    obj.movePath('food/mexican/tacos', 'fruits')

    expect(obj.path).toEqual({ food: { mexican: {} }, fruits: { tacos: {} } })
  })

  test('should fail moving a path when the destination does not exist', () => {
    const obj = new Tree()
    obj.addPath('food/mexican/tacos')
    obj.movePath('food/mexican/tacos', 'fruits/mexican')

    expect(consoleSpy).toHaveBeenCalledWith('Cannot move food/mexican/tacos - fruits does not exist')
  })

  test('should remove a path', () => {
    const obj = new Tree()
    obj.addPath('food/mexican/tacos/sesos')
    obj.addPath('food/mexican/tacos/ribs')
    obj.removePath('food/mexican/tacos/ribs')

    expect(obj.path).toEqual({ food: { mexican: { tacos: { sesos: {} } } } })
  })

  test('should fail removing a path when the path does not exist', () => {
    const obj = new Tree()
    obj.addPath('food/mexican/tacos/sesos')
    obj.removePath('food/mexican/soups')

    expect(consoleSpy).toHaveBeenCalledWith('Cannot delete food/mexican/soups - soups does not exist')
  })

  test('should list all paths in tree', () => {
    const obj = new Tree()
    obj.addPath('food/american/burritos')
    obj.addPath('food/mexican/tacos/sesos')
    obj.addPath('food/mexican/tacos/ribs')
    obj.addPath('fruits')

    const list = obj.listPaths()

    expect(list).toBe('food\r\n  american\r\n    burritos\r\n  mexican\r\n    tacos\r\n      ribs\r\n      sesos\r\nfruits\r\n')
  })
})
