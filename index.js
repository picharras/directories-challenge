const Tree = require('./Tree')
const commandsString = `
CREATE fruits
CREATE vegetables
CREATE grains
CREATE fruits/apples
CREATE fruits/apples/fuji
LIST
CREATE grains/squash
MOVE grains/squash vegetables
CREATE foods
MOVE grains foods
MOVE fruits foods
MOVE vegetables foods
LIST
DELETE fruits/apples
DELETE foods/fruits/apples
LIST
`
const tree = new Tree()

const commands = commandsString.split('\n')

for (const line of commands) {
  const [command, path1, path2] = line.split(' ')
  console.log(line)
  switch (command.toLowerCase()) {
    case 'create':
      tree.addPath(path1)
      break
    case 'list':
      console.log(tree.listPaths())
      break
    case 'move':
      tree.movePath(path1, path2)
      break
    case 'delete':
      tree.removePath(path1)
      break
  }
}
